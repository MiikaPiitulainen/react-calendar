export const headerStyle = {
  position: "relative",
  display: "flex",
  flexWrap: "wrap",
  width: "100%",
  height: "60px",
  borderBottom: "solid 1px #000000"
}

export const headerContentLeft = {
  fontWeight: "600",
  position: "absolute",
  left: "60px",
  top: "50%",
  transform: "translateY(-50%)"
}

export const headerContentRight = {
  position: "absolute",
  right: "60px",
  top: "50%",
  transform: "translateY(-50%)"
}

export const button = {
  position: "relative",
  width: "100px",
  height: "40px",
  borderRadius: "5px",
  backgroundColor: "#FFFFFF"
}