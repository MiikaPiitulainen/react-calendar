import React from 'react';
import * as style from './NavigationStyle';

const Navigation = () => {

    return (
        <div style={style.headerStyle}>
            <div style={style.headerContentLeft}>My calendar</div>
            <div style={style.headerContentRight}>
                <button style={style.button}>
                    <a href="index.html">Log out</a> 
                </button>
            </div>
        </div>
    );
}

export default Navigation;