import React from 'react';
import * as style from './NavigationStyle';

const Navigation = () => {
   



    return (
        <div style={style.NavigationStyle}>
            <div style={style.NavigationContentLeft}>Logo</div>
            <div style={style.NavigationContentRight}>
                <button style={style.button}>
                  <a href="index.html">Log out</a>
                </button>
            </div>
        </div>
    );
}

export default Navigation;