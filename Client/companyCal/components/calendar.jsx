import React, { useState } from "react";
import styles from "./calendarStyle.scss";
import CalendarColumn from "./calendarColumn"




const Calendar = function (props) {

    return (
       
        <div id="ccontainer">
            <div id="daySchedule">
                <div className="hDay" >

                    <div style={{ marginTop: "75px", borderBottom: "#78F0E0", borderBottomStyle:"dashed", borderBottomWidth: "3px"}}></div>

                    <div className="weekdayStaticHour">7:00</div>
                    <div className="weekdayStaticHour">8:00</div>
                    <div className="weekdayStaticHour">9:00</div>
                    <div className="weekdayStaticHour">10:00</div>
                    <div className="weekdayStaticHour">11:00</div>
                    <div className="weekdayStaticHour">12:00</div>
                    <div className="weekdayStaticHour">13:00</div>
                    <div className="weekdayStaticHour">14:00</div>
                    <div className="weekdayStaticHour">15:00</div>
                    <div className="weekdayStaticHour">16:00</div>
                    <div className="weekdayStaticHour">17:00</div>
                    <div className="weekdayStaticHour">18:00</div>
                </div>

                <CalendarColumn weekday="Maanantai"  shiftInfo={props.allShifts.monday}/>
                <CalendarColumn weekday="Tiistai" shiftInfo={props.allShifts.tuesday}/>
                <CalendarColumn weekday="Keskiviikko" shiftInfo={props.allShifts.tuesday}/>
                <CalendarColumn weekday="Torstai" shiftInfo={props.allShifts.tuesday}/>
                <CalendarColumn weekday="Perjantai" shiftInfo={props.allShifts.tuesday}/>
                <CalendarColumn weekday="Lauantai" shiftInfo={props.allShifts.tuesday}/>
                <CalendarColumn weekday="Sunnuntai" shiftInfo={props.allShifts.tuesday}/>
            </div>
        </div>

    );
};

export default Calendar;