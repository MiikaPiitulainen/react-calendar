import React from 'react';

export const EmployeeArrowLeft = (props) => {
    const handleClick = (setAppState) => {
        setAppState(prev => {
            const newState = {...prev};
            const ind = newState.employees.findIndex(emp => emp.name === prev.selectedEmployee);
            if (ind === 0) {
                newState.selectedEmployee = newState.employees[newState.employees.length - 1].name;
            } else {
                newState.selectedEmployee = newState.employees[ind-1].name;
            }
            return newState;
        });
    }
    const style = {
        position: "absolute",
        left: "-25px",
        top: "50%",
        transform:
        "translateY(-50%)"
    };

    return (
        <svg
            height="20"
            width="20"
            style={style}
            onClick={() => handleClick(props.setAppState)}>
            <polygon points="0,10 20,0 20,20" fill="#000" strokeWidth="1px"/>
        </svg>
    )
}

export const EmployeeArrowRight = (props) => {
    const handleClick = (setAppState) => {
        setAppState(prev => {
            const newState = {...prev};
            const ind = newState.employees.findIndex(emp => emp.name === prev.selectedEmployee);
            if (ind === newState.employees.length - 1) {
                newState.selectedEmployee = newState.employees[0].name;
            } else {
                newState.selectedEmployee = newState.employees[ind+1].name;
            }
            return newState;
        });
    }

    const style = {
        position: "absolute",
        right: "-25px",
        top: "50%",
        transform:
        "translateY(-50%)"
    };

    return (
        <svg
            height="20"
            width="20"
            style={style}
            onClick={() => handleClick(props.setAppState)}>
            <polygon points="0,20 0,0 20,10" fill="#000" strokeWidth="1px"/>
        </svg>
    )
}

export const WeekArrowLeft = (props) => {
    const handleClick = (setAppState) => {
        setAppState(prev => {
            const newState = {...prev};
            newState.displayedWeek = prev.displayedWeek - 1;
            return newState;
        });
    }

    const style = {
        position: "absolute",
        left: "-25px",
        top: "50%",
        transform:
        "translateY(-50%)"
    };

    return (
        <svg
            height="20"
            width="20"
            style={style}
            onClick={() => handleClick(props.setAppState)}>
            <polygon points="0,10 20,0 20,20" fill="#000" strokeWidth="1px"/>
        </svg>
    )
}

export const WeekArrowRight = (props) => {
    const handleClick = (setAppState) => {
        setAppState(prev => {
            const newState = {...prev};
            newState.displayedWeek = prev.displayedWeek + 1;
            return newState;
        });
    }

    const style = {
        position: "absolute",
        right: "-25px",
        top: "50%",
        transform:
        "translateY(-50%)"
    };

    return (
        <svg
            height="20"
            width="20"
            style={style}
            onClick={() => handleClick(props.setAppState)}>
            <polygon points="0,20 0,0 20,10" fill="#000" strokeWidth="1px"/>
        </svg>
    )
}