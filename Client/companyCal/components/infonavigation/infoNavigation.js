import React from 'react';
import * as style from './infoNavigationStyle';
import { EmployeeArrowLeft, EmployeeArrowRight, WeekArrowLeft, WeekArrowRight} from './svgArrows';

const InfoNavigation = (props) => {
    return (
        <div style={style.infoNavigationStyle}>
            <div style={style.NavigationContentLeft}>
                <WeekArrowLeft setAppState={props.setAppState}/>
                <button style={style.button}>
                    Week {props.currentWeek}
                </button>
                <WeekArrowRight setAppState={props.setAppState}/>
            </div>
            <div style={style.NavigationContentMiddle}>{props.companyName}</div>
            <div style={style.NavigationContentRight}>
                <EmployeeArrowLeft setAppState={props.setAppState}/>
                <button style={style.button}>
                    {props.selectedEmployee}
                </button>
                <EmployeeArrowRight setAppState={props.setAppState}/>
            </div>
        </div>
    );
}

export default InfoNavigation;