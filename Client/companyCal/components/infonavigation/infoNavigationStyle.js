export const infoNavigationStyle = {
    position: "relative",
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    height: "60px",
    borderBottom: "solid 1px #000000",
    height: "10vh"
}

export const NavigationContentLeft = {
    position: "absolute",
    left: "60px",
    top: "50%",
    transform: "translateY(-50%)"
};

export const NavigationContentMiddle = {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translateX(-50%) translateY(-50%)"
};

export const NavigationContentRight = {
    position: "absolute",
    right: "60px",
    top: "50%",
    transform: "translateY(-50%)"
};

export const button = {
    position: "relative",
    width: "100px",
    height: "40px",
    borderRadius: "5px",
    backgroundColor: "#FFFFFF",
};