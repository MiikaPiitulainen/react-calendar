import * as React from "react";
// import "Styles/main.scss";
import Calendar from './components/calendar'
import Navigation from './components/navigation/navigation.js'
import InfoNavigation from './components/infonavigation/infoNavigation.js'

function App() {

    const [companyShifts, setCompanyShifts] = React.useState({
        company: "random",
        selectedEmployee: "random",
        displayWeek: "",
        employees: [
            {
                name: "rene Orsosz",
                shifts:
                {
                    monday:
                        [{
                            startingMinute: 420,
                            endingMinute: 430,
                            reviewed: false,
                            accepted: false
                        },
                        {
                            startingMinute: 480,
                            endingMinute: 490,
                            reviewed: false,
                            accepted: false
                        }],
                    tuesday:
                        [{
                            startingMinute: 30,
                            endingMinute: 60,
                            reviewed: false,
                            accepted: false
                        }]
                }

            }
        ]

    });
    return (
        <div className="App">
            <div className="container bg-light">
                <Navigation />
                <InfoNavigation/>
                <Calendar allShifts={companyShifts.employees[0].shifts}/>
            </div>
        </div>
    );
}

export default App;
