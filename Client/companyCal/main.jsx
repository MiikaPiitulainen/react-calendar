import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

const render = (Component) => {
    ReactDOM.render(
        //<Provider store={store}>
        <Component />,
        //</Provider>
        document.getElementById("root")
    );
};

render(App);

if (module.hot) {
    module.hot.accept("./app", () => {
        const newApp = require("./app").default;
        render(newApp);
    });
}
