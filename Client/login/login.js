const SUCCESS_STATUS = 200;

//when log in, this gives you error message if there are error in login.
// #error-text located in index.html
function hideError() {
    const error_text = document.querySelector("#error_text");
    error_text.style.display = "none";
    console.log("kirjautuminen");
}

//Sending login info to server,
// if error occurs gives error message if login success shows you the calendar page
async function login() {
    hideError();

    const form = document.querySelector("#login_form");
    const data = new FormData(form);
    const response = await httpPost("/api/user/login", data);

    if (response.status == SUCCESS_STATUS) {
        window.location = "/app.html";
    } else {
        error_text.style.display = "block";
    }
}

//Register new user and send form data to server. if error occurs gives error message.
async function register() {
    hideError();

    const form = document.querySelector("#register_form");
    const data = new FormData(form);
    const response = await httpPost("/api/user/register", data);

    if (response.status === SUCCESS_STATUS) {
        window.location = "/app.html";
    } else {
        error_text.innerHTML = await response.text();
        error_text.style.display = "block";
    }
}

//you can use this function to send
//an object to the server
async function httpPost(url, data) {
    const response = await fetch(url, {
        method: "POST",
        body: data,
    });

    return response;
}
