const { resolve } = require("path");

const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

const webpackMode = "development";

const config = {
    stats: {
        maxModules: 0
    },
    mode: webpackMode,
    devtool: "cheap-module-eval-source-map",

    entry: {
        main: [
            "../src/assets/js/main.js",
            "../src/assets/scss/main.scss",
            "webpack-dev-server/client?http://localhost:8080",
            "webpack/hot/only-dev-server"
        ],
        react: [
            "../companyCal/main.jsx", // For the react stuff
            "../companyCal/assets/less/main.scss", // For the react stuff
            "webpack-dev-server/client?http://localhost:8080",
            "webpack/hot/only-dev-server"
        ]
    },

    output: {
        filename: "[name].js",
        path: resolve(__dirname, "build"),
        publicPath: "",
    },

    context: resolve(__dirname, "companyCal"),

    devServer: {
        hot: true,
        contentBase: resolve(__dirname, "build"),
        historyApiFallback: true,
        publicPath: "/",
        port: 8080,
        watchOptions: {
            poll: true
        },
        proxy: {
            "/api": "http://localhost:4000",
        }
    },

    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            deepmerge$: path.resolve(__dirname, "node_modules/deepmerge/dist/umd.js"),
            Images: path.resolve(__dirname, "app/assets/images/"),
            Videos: path.resolve(__dirname, "app/assets/videos/"),
            Styles: path.resolve(__dirname, "app/assets/less/"),
            "../../theme.config$": path.join(__dirname, "app/theme/theme.config")
        }
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loaders: [
                    "babel-loader",
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.html/,
                loaders: [
                    "html-loader"
                ]
            },
            {
                test: /\.css$/i,
                loaders: [
                    "style-loader",
                    {
                        loader: "css-loader", //generating unique classname
                        options: {
                            importLoaders: 1, // if specifying more loaders
                            modules: true,
                            sourceMap: false,
                            localIdentName: "[path]___[name]__[local]___[hash:base64:5]" //babel-plugin-css-module format
                        }
                    }
                ]
            },
            
            {
                test: /\.scss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            mimetype: "image/png",
                            name: "images/[name].[ext]",
                        }
                    }
                ],
            },
            {
                test: /\.eot(\?v=\d+.\d+.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "fonts/[name].[ext]"
                        }
                    }
                ],
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            mimetype: "application/font-woff",
                            name: "fonts/[name].[ext]",
                        }
                    }
                ],
            },
            {
                test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            mimetype: "application/octet-stream",
                            name: "fonts/[name].[ext]",
                        }
                    }
                ],
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                            mimetype: "image/svg+xml",
                            name: "images/[name].[ext]",
                        }
                    }
                ],
            },
        ]
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.LoaderOptionsPlugin({
            test: /\.jsx?$/,
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new MiniCssExtractPlugin({ filename: "[name].[contenthash].css" }),
        new WriteFilePlugin(),
        new CopyWebpackPlugin([
            { from: resolve(__dirname, "./login/"), to: resolve(__dirname, "./build/") },
            { from: resolve(__dirname, "./src/html/"), to: resolve(__dirname, "./build/") },
            { from: resolve(__dirname, "./companyCal/companyCal.html"), to: resolve(__dirname, "./build/") },
            // Used after integrating the project to server
            // { from: resolve(__dirname, "./login/"), to: resolve(__dirname, "./build/public/") },
            // { from: resolve(__dirname, "./src/html/features"), to: resolve(__dirname, "./build/public/") },
            // { from: resolve(__dirname, "./src/html/title.html"), to: resolve(__dirname, "./build/public/") },
            // { from: resolve(__dirname, "./src/html/app.html"), to: resolve(__dirname, "./build/") },
            // { from: resolve(__dirname, "./companyCal/companyCal.html"), to: resolve(__dirname, "./build/") },
        ]),
        new webpack.HotModuleReplacementPlugin()
    ]
};

module.exports = config;