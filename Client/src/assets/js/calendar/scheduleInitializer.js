import * as ploppedTimes from "./times/ploppedTimes";

const defaultWeek = [
    "H",
    "Maanantai",
    "Tiistai",
    "Keskiviikko",
    "Torstai",
    "Perjantai",
    "Lauantai",
    "Sunnuntai"
];

const formatDate = (date) => {
    const yyyy = date.getFullYear();
    let dd = date.getDate();
    let mm = date.getMonth() + 1;

    if (dd < 10) {
        dd = "0" + dd;
    }

    if (mm < 10) {
        mm = "0" + mm;
    }

    return dd + "/" + mm + "/" + yyyy;
};

const moveWeekBackwards = (date) => {
    date.setDate(date.getDate() - 2);
    date.setDate(date.getDate() - date.getDay() + 1);

    defaultWeek.slice(1, defaultWeek.length).forEach((day) => {
        const selectedEpochDate = date.getTime() / 1000;
        document.getElementById(day).setAttributes({date: selectedEpochDate});

        createDayHeaderElement(selectedEpochDate, day);
        date.setDate(date.getDate() + 1);
    });
};

const moveWeekForwards = (date) => {
    
    date.setDate(date.getDate() + 1);

    defaultWeek.slice(1, defaultWeek.length).forEach((day) => {
        const selectedEpochDate = date.getTime() / 1000;
        document.getElementById(day).setAttributes({date: selectedEpochDate});

        createDayHeaderElement(selectedEpochDate, day);
        date.setDate(date.getDate() + 1);
    });
};

const setHeaderClick = (id, element, date) => {
    const previousweek = document.getElementById("previousweek");
    const nextweek = document.getElementById("nextweek");

    if (id === "Maanantai") {
        previousweek.onclick = () => {
            
            moveWeekBackwards(date);
            
        };
    } else if (id === "Sunnuntai") {
        nextweek.onclick = () => {
            moveWeekForwards(date);
        };
    }
};

export const setFirstChild = (id, element) => {

    const coreElement = document.getElementById(id);
    if (!(coreElement.children.length > 0)) {
        coreElement.appendChild(element);
    } else {
        coreElement.removeChild(coreElement.children[0]);
        coreElement.insertBefore(element, coreElement.children[0]);
    }
};

const defaultDayLength = 24;

const scheduleInitialize = (
    domScheduleIdentifier, week = defaultWeek,
    dayLength = defaultDayLength
) => {
    if (domScheduleIdentifier) {
        const schedule = document.getElementById(domScheduleIdentifier);

        if (schedule.childElementCount === week.length) {
            // Rewind the current date back to the monday of current week
            const thisDate = new Date();
            console.log(thisDate);
            thisDate.setDate(thisDate.getDate() - thisDate.getDay());
            console.log(thisDate);

            [...schedule.children].forEach((child, index) => {

                // Give all weekday columns a epoch timestamp
                const epochDate = thisDate.getTime() / 1000;
                if (index > 0) {
                    child.setAttributes({ date: epochDate });
                }

                // Create the inner DOM for the weekday column
                createDayHeaderElement(epochDate, week[index]);
                child.appendChild(document.createElement("div"));
                populateDayWithHours(week[index], dayLength).forEach((hourDiv) => {
                    child.appendChild(hourDiv);
                });

                thisDate.setDate(thisDate.getDate() + 1);
            });
        } else {
            console.error("´week´ parameters weekday array length has to match  the count of domScheduleIdentifier elements children.");
        }
    }
};


const createDayHeaderElement = (date, weekday) => {
    ploppedTimes.clearPlops();
    

    const div = document.createElement("div");
    const insideDiv = document.createElement("div");

    const titleParagraph = document.createElement("p").setAttributes({class: "titleParagraph"});
    titleParagraph.innerHTML = weekday;
    insideDiv.appendChild(titleParagraph);

    if (weekday === "H") {
        insideDiv.setAttribute("id", "hourHeaderContent");
    } else {
        const dayDetailParagraph = document.createElement("p").setAttributes({class: "dayDetailParagraph"});
        const thisWeekdayDate = new Date(0);
        

        thisWeekdayDate.setUTCSeconds(+date);
        
        dayDetailParagraph.innerHTML = formatDate(thisWeekdayDate);
        insideDiv.appendChild(dayDetailParagraph);

        setHeaderClick(weekday, div, thisWeekdayDate);
    }

    div.appendChild(insideDiv);

    div.setAttributes({ id: `header-${weekday.replace(/\s/g, '')}`, class: "weekdayHeader" }); // checking later, IE support?

    setFirstChild(weekday, div);

    if (weekday === "Sunnuntai"){
        const weekdays = [
            "Maanantai", "Tiistai", "Keskiviikko",
            "Torstai", "Perjantai", "Lauantai", "Sunnuntai"
        ];
        weekdays.forEach((day)=>{
            const weekdayHeader = document.getElementById("header-"+ day);
            const currentDate = weekdayHeader.children[0].children[1].innerHTML;
            ploppedTimes.insertNewPlops(currentDate, day);

        }
        )
        
    }
    
};

const populateDayWithHours = (weekday, hoursInDay, isHourList = false) => {
    
    
    const hourDivList = []; // not HIvalue fix
    const hourDiv = document.createElement("div");
    hourDiv.setAttribute("class", (isHourList) ? "weekdayHourNumber" : "weekdayStaticHour");

    for (let i = 0; i < hoursInDay; i++) {
        if (i !== 0) { hourDiv.removeAttribute("id"); }
        hourDiv.setAttribute("id", `staticHour-${weekday}-${i}`);
        hourDivList.push(hourDiv.cloneNode(true));

        hourDivList[hourDivList.length - 1].onclick = (ev) => {
            ev.preventDefault();
            const dateElement = ev.target.parentElement.children[0].children[0].children[1];
            const date = dateElement.innerHTML;
            //puuttuu mikä päivämäärä on.

            ploppedTimes.timePlopper(i, weekday, date);
        };
    }

    return hourDivList;
};


export default scheduleInitialize;
