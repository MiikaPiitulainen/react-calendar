export default class PlopExpander {

    constructor(id, e, updateStoredTime) {
        e.stopPropagation();
        this.mouseMoved.bind(this);
        this.mouseUp.bind(this);
        this.syncStates.bind(this);

        this.expandStoredTime = updateStoredTime;
        this.syncStates(e);

        this.timeRootElement = document.getElementById(id);
    }

    syncStates(e) {
        this.startY = e.y;

        this.delayY = this.startY;

        document.onmousemove = (e) => this.mouseMoved(e);  //not required in JS?
        document.onmouseup = (e) => this.mouseUp(e);
    }

    mouseMoved(e) {
        this.liveY = e.y;
        const yChange = this.liveY - this.delayY;
        this.delayY = e.y;
        const newParentHeight = this.timeRootElement.offsetHeight + yChange;

        this.expandStoredTime(this.timeRootElement.offsetTop - 50, newParentHeight);
        this.timeRootElement.setAttributes({
            styles: {
                height: `${(newParentHeight >= 25) ? newParentHeight : 25}px`
            }
        });

    }

    mouseUp(e) {
        document.onmousemove = null;
        document.onmouseup = null; 
    }
}