export default class PlopMover {

    constructor(id, e, updateStoredTime) {
        this.mouseMoved.bind(this);
        this.mouseUp.bind(this);
        this.syncStates.bind(this);

        this.moveStoredTime = updateStoredTime;
        this.syncStates(e);

        this.id = id;

        this.timeRootElement = document.getElementById(id);
    }

    syncStates(e) {

        this.startY = e.y;

        this.delayY = this.startY;

        document.onmousemove = (e) => this.mouseMoved(e); //not required in JS?
        document.onmouseup = (e) => this.mouseUp(e);
        document.onwheel = (e) => this.mouseMoved(e);
    }

    mouseMoved(e) {
        this.checkXmovement(e);
        this.liveY = e.y;
        const yChange = this.liveY - this.delayY;
        this.delayY = e.y;

        const scrollAmount = document.getElementById("daySchedule").scrollTop;
        let scroll = e.deltaY;

        if (e.deltaY === undefined || scrollAmount < 54 || scrollAmount > 1370) {
            scroll = 0;
        }

        const newParentMargin = (+this.timeRootElement.style.marginTop.slice(
            0, this.timeRootElement.style.marginTop.length - 2
        )) + yChange + scroll;


        // Plopin rajaus. normaalisti ploppi pysähtyy 24 tunnin kohdalla
        // paitsi jos sitä on expandattu
        const minMargin = 49.5;
        const maxMargin = 1430;
        if (newParentMargin < maxMargin){
            this.moveStoredTime(newParentMargin - minMargin);
            this.timeRootElement.setAttributes({
                styles: {
                    "margin-top": `${(newParentMargin >= minMargin) ? newParentMargin : minMargin}px`
                    }
                });
            }
    }

    mouseUp() {
        document.onmousemove = null;
        document.onmouseup = null;
        document.onwheel = null;
    }

    checkXmovement(e){
        //console.log(e.x);
        const target = e.target.parentElement;
        if(target===null){
            return;
        }
        const weekdays = [
            "Maanantai", "Tiistai", "Keskiviikko",
            "Torstai", "Perjantai", "Lauantai", "Sunnuntai"
        ];
        weekdays.forEach((day)=>{
            if(day===target.id){
                target.insertBefore(this.timeRootElement, target.children[1]);
            }
        })
    }
}