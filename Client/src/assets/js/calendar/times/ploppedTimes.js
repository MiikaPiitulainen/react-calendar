/* eslint-disable import/no-mutable-exports */
import PlopExpander from './plopInteractions/plopExpanding.js';
import PlopMover from "./plopInteractions/plopMoving.js";

export let ploppedTimes = [];

const storePloppedTime = (id, startTime, date) => {
    const clearWeek = document.getElementById("clearweek");
    const ploppedItems = document.getElementsByClassName("timeRootDiv")

    console.log(startTime);
    ploppedTimes.push({
        id,
        beginTime: startTime,
        endTime: startTime + 60,
        date
    
    }); 
    console.log(ploppedTimes)
    clearWeek.onclick = () => {
        clearWeek.style.background = "#7ef54f";
        console.log("asdasdasdasds")
        console.log(ploppedTimes.length);
        ploppedTimes.splice(0,1000)
    };
};
const forgetPloppedTime = (id) => {
    //luo uuden taulukon joka täyttää nämä ehdot
    ploppedTimes = ploppedTimes.filter(ploppedTime => ploppedTime.id !== id);
    };
// Convert minutes in to HH:MM
const displayProperTime = (sumMinutes) => {
    const actualHours = `${Math.floor(sumMinutes / 60)}`;
    //console.log(sumMinutes, actualHours);
    const zeroedHours = `${(actualHours.length > 1) ? "" : "0"}${actualHours}`;

    const actualMinutes = `${sumMinutes % 60}`;
    const zeroedMinutes = `${(actualMinutes.length > 1) ? "" : "0"}${actualMinutes}`;

    return `${zeroedHours}:${zeroedMinutes}`;
};
// Construct the time details to DOM, eg time plop start time and end time.
const constructTimeDetails = (id) => {

    const storedPloppedTime = ploppedTimes.find(ploppedTime => ploppedTime.id === id);

    const timeDetailContainer = document.createElement("div").setAttributes({
        class: "timeDetailContainer"
    });

    const timeDetails = document.createElement("p");
    timeDetails.textContent = `${
        displayProperTime(storedPloppedTime.beginTime)
    } - ${
        displayProperTime(storedPloppedTime.endTime)
    }`;

    timeDetailContainer.appendChild(timeDetails);

    const ploppedTimeDom = [...document.getElementById(id).childNodes][0];

    if (!(ploppedTimeDom.childElementCount < 2)) {
        ploppedTimeDom.removeChild(ploppedTimeDom.childNodes[0]);
    }

    ploppedTimeDom.insertBefore(
        timeDetailContainer,
        ploppedTimeDom.firstChild
    );

};

// Constructs the time plop expander DOM, the details and functionality.
const constructPloppedTimeExpander = (id) => {

    const timeExpander = document.createElement("div");
// When clicked(drag) the time plop´s expand button change color.
    timeExpander.setAttributes({ class: "timeExpander" });
    timeExpander.addEventListener("click", ()=>{
        timeExpander.style.backgroundColor = "blue";
    })
    timeExpander.onmousedown = (e) => {

        new PlopExpander(id, e, (newBeginTime, newEndTime) => {
            const correctPloppedTimeIndex = ploppedTimes.findIndex((ploppedTime) => ploppedTime.id === id);
            console.log(correctPloppedTimeIndex);
            ploppedTimes[correctPloppedTimeIndex].endTime = ploppedTimes[correctPloppedTimeIndex].beginTime + newEndTime;
            ploppedTimes[correctPloppedTimeIndex].beginTime = newBeginTime;

            constructTimeDetails(id);
        });

    };

    return timeExpander;
};

// Constructs a DOM from the plopped time details
const ploppedTimeToDom = (id, hour, day) => {
    if (day !== "H") {

        const beginTime = hour * 60;

        const dayHourList = document.getElementById(day);
        const timeRootDiv = document.createElement("div");

    
        timeRootDiv.setAttributes({
            class: "timeRootDiv",
            id,
            styles: {
                "margin-top": `${beginTime + 49.5}px`,
                height: "60px",//laatikon koko
                color:"red"
               
            }
            //tänne tehdäään raksikohta, kun raksi tulee lisätä. Kun raksin lisää nii pitää hävitä laatikko samalla kertaa.
            //
        });




        const timeContentDiv = document.createElement("div");

        // Construct the plop moving functionality and DOM. 
        timeContentDiv.appendChild(constructPloppedTimeExpander(id));

        timeContentDiv.onmousedown = (e) => {

            console.log(e.button);
            if (e.button === 0) {
                new PlopMover(id, e, (newBaseTime) => {
                    console.log(newBaseTime);
                    const correctPloppedTimeIndex = ploppedTimes.findIndex((ploppedTime) => ploppedTime.id === id);

                    ploppedTimes[correctPloppedTimeIndex].endTime = newBaseTime + (ploppedTimes[correctPloppedTimeIndex].endTime - ploppedTimes[correctPloppedTimeIndex].beginTime);
                    ploppedTimes[correctPloppedTimeIndex].beginTime = newBaseTime;
                    constructTimeDetails(id);
                });

            }

        };

        timeRootDiv.appendChild(timeContentDiv);

        dayHourList.insertBefore(timeRootDiv, dayHourList.children[1]);
        constructTimeDetails(id);
    }
};

export const timePlopper = (hour, day, date) => {
    
    const plopTimeId = `${date} ${Math.floor((1000000 * Math.random()))}${Date.now()}`;
    
    
    storePloppedTime(plopTimeId, hour * 60, date);
    ploppedTimeToDom(plopTimeId, hour, day);
};

export const clearPlops = function clearPlops(){
    const plops = document.getElementsByClassName("timeRootDiv");
    

    for (let i = 0; i<plops.length; i++){
        
        plops[i].remove();
        
    }
}
export const insertNewPlops = function insertNewPlops(date, day){
   ploppedTimes.forEach((plop)=>{
    if(date === plop.date){
            ploppedTimeToDom(plop.id, plop.beginTime/60, day)
            console.log("asd");
    }
   })
   
}