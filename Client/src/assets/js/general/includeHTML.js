const getHtmlContent = function getHtmlContent(file) {
    let xhttp;
    return new Promise((resolve) => {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function onreadystatechange() {
            if (this.readyState === 4) {
                if (this.status === 200) { resolve(this.responseText); }
                if (this.status === 404) { resolve("Page not found."); }
            }
        };
        xhttp.open("GET", file, true);
        xhttp.send();
    });
};

const includeHTML = async function includeHTML() {
    let elmnt;
    let file;
    const allFileContents = [];
    const allElements = [];
    // Loop through a collection of all HTML elements:
    const z = document.getElementsByTagName("div");
    for (let i = 0; i < z.length; i++) {
        elmnt = z[i];
        //search for elements with a certain atrribute:
        file = elmnt.getAttribute("w3-include-html");
        if (file) {
            allFileContents[i] = getHtmlContent(file);
            allElements[i] = elmnt;

            // Remove the attribute, and call this function once more:
            elmnt.removeAttribute("w3-include-html");
        }
    }

    const finalContents = await Promise.all(allFileContents);
    for (let i = 0; i < allElements.length; i++) {
        if (allElements[i] !== undefined) {
            allElements[i].innerHTML = finalContents[i];
        }
    }
};

export default includeHTML;
