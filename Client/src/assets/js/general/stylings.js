
//Exporting function can be used in other programs, calendar properties CSS modified with getComputerstyle and getPropertyValue
export const pixelizeCalendarWidth = () => {
    const currentContainerWidth = getComputedStyle(document.getElementById("ccontainer"))
        .getPropertyValue("width");

    //chaning calendar weekdays width
    let varientWidth =  +currentContainerWidth.slice(0, currentContainerWidth.length - 2);
    varientWidth -= 20; 

    const dayScheduleWidth = `${varientWidth}px`;

    document.getElementById("daySchedule").style.setProperty("width", dayScheduleWidth);

    varientWidth -= 15;
    //Changing Calendar css wiw
    const sDayWidth = `calc((${varientWidth - varientWidth / 30}px / 7))`;
    [...document.getElementsByClassName("sDay")].forEach(dayElement => {
        dayElement.style.setProperty("width", sDayWidth);
    });
    //chaning calendar weekdays width
    const hDayWidth = `calc(${varientWidth / 30}px)`;
    document.getElementsByClassName("hDay")[0].style.setProperty("width", hDayWidth);

};

//Modified calender width when screen resizes
export const normalizeRules = () => {
    window.addEventListener("resize", (e) => {
        pixelizeCalendarWidth();
    });
}

export const setAttributes = function (attrs) {
    Object.keys(attrs).forEach((attr) => {
        if (attr === "styles" && typeof attrs[attr] === "object") {
            const styles = attrs[attr];
            Object.keys(styles).forEach((style) => {
                this.style.setProperty(style, styles[style]);
            });
        } else {
            this.setAttribute(attr, attrs[attr]);
        }
    });

    return this;
};