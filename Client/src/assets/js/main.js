//import "@babel/polyfill";
import { setAttributes } from './general/stylings';
import scheduleViewInitializer from './calendar/scheduleInitializer';
import * as applyStyling from './general/stylings.js';
import includeHTML from './general/includeHTML.js';

Element.prototype.setAttributes = setAttributes;

includeHTML()
    .then(() => {
        scheduleViewInitializer("daySchedule");
        applyStyling.pixelizeCalendarWidth();
        applyStyling.normalizeRules();
    });
