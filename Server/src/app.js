/**
 * -- WARNING --
 * Not exactly properly handled back end but serves its' purpose
 * for this course.
 */
import express from "express";
import bodyParser from "body-parser";
import expressSession from 'express-session';

import publicRoutes from './routes/public.routes.js';
import loggedInRoutes from './routes/loggedIn.routes.js';
import authenticationRoutes from './routes/authenticate.routes.js';

// import User from "./models/user.model";
const app = express();
const {
    port = 4000,
    // todo: Define elsewhere
    sessionIdSecret = "aSecretStringThatShouldBeChangedInProduction",
    sessionCookieName = "cookieNameHere",
    cookieTime = 1000 * 60 * 2, //Milliseconds, --> 1000*60*2 milliseconds = 120 seconds = 2 minutes
} = process.env;
export { cookieTime };

/**
 * Generally you want to use session storage as well
 * and there are good implementations for those already.
 * The implementation used here is not something i would recommend.
 */
app.use(expressSession({
    name: sessionCookieName, // cookie name
    resave: false, 
    saveUninitialized: false,
    rolling: true, // reset cookie on every response
    secret: sessionIdSecret,
    cookie: {
        secure: false, // In production this should always be true! It, however, requires HTTPS to work. 
        maxAge: cookieTime, // 1000 Milliseconds * N == seconds
        sameSite: true,
        //domain: 'example.com',
        //path: 'foo/bar',
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files
app.use("/", express.static("../Client/build"));

// Middleware "/" used for debugging
app.use("/", (request, response, next) => {
    // console.log(request.session);
    // console.log(request.url);
    next();
});

// Serve dynamics
app.use("/", publicRoutes);
app.use("/", loggedInRoutes);
app.use("/", authenticationRoutes);

// API Control
// app.use('/api/user/', userRouter); // tobe implemented

app.listen(port, () => console.log(`Backend API listening on port ${port}!`));
