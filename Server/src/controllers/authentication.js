import fs from 'fs';

// Keep track of cookies and what user they correspond to. Delete
// cookie ID from database if the cookie timer runs out.
let cookieTimeouts = {};
const updateCookie = (id, cookieTime) => {
    if (!id || !cookieTime) return;

    if (cookieTimeouts[id.toString()]) { clearTimeout(cookieTimeouts[id.toString()]); }

    cookieTimeouts[id.toString()] = setTimeout(() => {
        const users = getUsersFromDb();
        const foundUser = users.findIndex(user => user.cookieId === id);
        users[foundUser].cookieId = null;

        const userData = JSON.stringify({ users });
        fs.writeFileSync('./src/database/users.database.json', userData);
    }, cookieTime);
}

// Delete cookie from "database"
const deleteCookieId = (cookieId) => {
    let userData = fs.readFileSync('./src/database/users.database.json');
    const { users } = JSON.parse(userData);

    const foundUser = users.findIndex(user => user.cookieId === cookieId);
    users[foundUser].cookieId = null;
        
    userData = JSON.stringify({ users });
    fs.writeFileSync('./src/database/users.database.json', userData);
    
    if (cookieTimeouts[cookieId.toString()]) 
        clearTimeout(cookieTimeouts[cookieId.toString()]);
}

const getUsersFromDb = () => {
    const userData = fs.readFileSync('./src/database/users.database.json');
    const { users } = JSON.parse(userData);
    return users;
}

export default {deleteCookieId, getUsersFromDb, updateCookie}