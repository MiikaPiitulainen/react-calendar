import { Router } from "express";
const router = Router();

/**
 * INDEX.HTML
 */
router.get('/index.html', (req,res) => {
    res.redirect('/');
});
router.get('/', (req,res) => {
    res.sendFile("index.html",{
        root: "../Client/build"
    });
});

/**
 * REGISTER.HTML
 */
router.get('/register.html', (req,res) => {
    res.sendFile("register.html",{
        root: "../Client/build"
    });
});

/**
 * FORGOTPASSWORD.HTML
 */
router.get('/forgotpassword.html', (req,res) => {
    res.sendFile("forgotpassword.html",{
        root: "../Client/build"
    });
});

export default router;