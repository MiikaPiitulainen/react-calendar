import { Router } from "express";
import fs from 'fs';
import auth from '../controllers/authentication.js';

const router = Router();
// todo: Define elsewhere
const sessionCookieName = "cookieNameHere";

// TODO: hash pw and create cookie id in a better fashion
/**
 * LOGIN
 */
router.post("/login", (req,res) => {
    const { username, password } = req.body;
    const users = auth.getUsersFromDb();

    const foundUser = users.findIndex(user => user.username === username && user.password === password);
    if (foundUser !== -1) {
        const cookieId = users[foundUser].username + "_" + Math.floor(Math.random()*100000).toString();
        req.session.cookieId = cookieId;
        users[foundUser].cookieId = cookieId;
        
        const userData = JSON.stringify({ users });
        fs.writeFileSync('./src/database/users.database.json', userData);

        res.redirect("/app");
    } else {
        console.log("not good...");
        res.redirect("/");
    }
});

/**
 * LOGOUT
 */
router.post("/logout", (req,res) => {
    req.session.destroy(err => {
        if (err) {
            resp.redirect("/app");
        } else {
            auth.deleteCookieId(req.session.cookieId);

            resp.clearCookie(sessionCookieName);
            resp.redirect("/");
        }
    });
});

/**
 * REGISTER
 */
router.post("/register", (req,res) => {
    // Make sure this is all that is needed
    const { username, password } = req.body;
    const users = auth.getUsersFromDb();

    const foundUser = users.findIndex(user => user.username === username);
    if (foundUser !== -1) {
        users.push({
            "username": username,
            "password": password,
            "cookieId": null,
        });
        res.redirect("/");
    } else {
        res.redirect("/register");
    }
});

export default router;