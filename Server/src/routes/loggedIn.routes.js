import { Router } from "express";
import auth from '../controllers/authentication.js';
import { cookieTime } from '../app.js';

const router = Router();

const authenticate = (request, response, next) => {
    const users = auth.getUsersFromDb();
    const foundUser = users.findIndex(user => user.cookieId === request.session.cookieId);
    
    if (foundUser !== -1) {
        auth.updateCookie(request.session.cookieId, cookieTime);
        next();
    } else {
        response.redirect("/");
    }
};

// To get this far the user needs to pass the log in middleware
router.get("/app", authenticate, (req,res) => {
    res.sendFile("app.html",{
        root: "../Client/build"
    });
});

router.get("/companyCal", authenticate, (req,res) => {
    res.sendFile("companyCal.html",{
        root: "../Client/build"
    });
});

export default router;