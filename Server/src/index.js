//allows you to use import and export by using esm modules
require = require("esm")(module);
module.exports = require("./app.js");