## How to configure

The server can be run by first making sure you've installed the libraries using

```
npm install
```

Then 

```
npm start
```

You need to configure your client side files a bit<br/>
First make sure your login form within **index.html** leads to "/login" and is a post method. Thus the form should be defined like the following

```
<form action="/login" method="post" id="login_form">
...all the other stuff...
</form>
```

On top of this the logout button within the app.html should be a post method action leading to "/logout".<br/>

Also, on the register.html page, the register form should be a post method leading to "/register".<br/>

Finally, on the forgotpassword.html page, the forgotpassword form should be a post method leading to "/forgotpassword".<br/>

Then we need to redefine some things in the webpack.config.js<br/>
First change

```
new CopyWebpackPlugin([
    { from: resolve(__dirname, "./login/"), to: resolve(__dirname, "./build/") },
    { from: resolve(__dirname, "./src/html/"), to: resolve(__dirname, "./build/") },
    { from: resolve(__dirname, "./companyCal/companyCal.html"), to: resolve(__dirname, "./build/") },
]),
```

to

```
new CopyWebpackPlugin([
    { from: resolve(__dirname, "./login/"), to: resolve(__dirname, "./build/public/") },
    { from: resolve(__dirname, "./src/html/features"), to: resolve(__dirname, "./build/public/") },
    { from: resolve(__dirname, "./src/html/title.html"), to: resolve(__dirname, "./build/public/") },
    { from: resolve(__dirname, "./src/html/app.html"), to: resolve(__dirname, "./build/") },
    { from: resolve(__dirname, "./companyCal/companyCal.html"), to: resolve(__dirname, "./build/") },
]),
```